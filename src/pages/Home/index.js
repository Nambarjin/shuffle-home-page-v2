import React from 'react'
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import BurgerMenu from '../../components/BurgerMenu'
import './Home.scss'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation } from 'swiper'

SwiperCore.use([Navigation]);

const Home = props => {
    return (
        <div id="home-page">
            <BurgerMenu />
            <header className="header">
                <img className="header__logo" src="/assets/logo.png" alt="logo" />
                <ul className="header__nav">
                    <li className="header__nav__link"><a href="/">overview</a></li>
                    <li className="header__nav__link"><a href="/">faqs</a></li>
                    <li className="header__nav__link"><a href="/">features</a></li>
                </ul>
                <div className="header__btns">
                    <a className="btn btn--header btn--lightblue" href="https://app.elify.com/signup?">get shuffle</a>
                    <a className="btn btn--header btn--blue" href="https://app.elify.com/#!/signin">sign in</a>
                </div>
            </header>

            <section className="hero">
                <div className="container">
                    <div className="hero__content">
                        <div className="hero__content__title">SIMPLIFIED SALES AND MARKETING FOR ENTREPRENEURS</div>
                        <div className="hero__content__text">BUILD YOUR BUSINESS AND DRIVE SALES LIKE A PRO</div>
                        <a className="btn btn--hero btn--lightblue" href="https://app.elify.com/signup?">try shuffle for $1</a>
                        <div className="disclaimer">No obligations, no contracts, cancel at any time.</div>
                    </div>
                </div>                
            </section>

        <section className="icons">
            <div className="container">
                <div className="hero__icons">
                    <div className="hero__icon">
                        <img src="./assets/hero-1.png" alt="icon" className="hero__icon__image" />
                        <p className="hero__icon__text">Always have the right materials on hand</p>
                    </div>
                    <div className="hero__icon">
                        <img src="./assets/hero-2.png" alt="icon" className="hero__icon__image" />
                        <p className="hero__icon__text">Never let a lead fall through the cracks</p>
                    </div>
                    <div className="hero__icon">
                        <img src="./assets/hero-3.png" alt="icon" className="hero__icon__image" />
                        <p className="hero__icon__text">See proof that your strategies work</p>
                    </div>
                </div>
            </div>
        </section>

        <section className="entr">
            <div className="card">
                <img className="card__img" src="/assets/entr.png" alt="" />
                <div className="right-fix">
                    <div className="card__content">
                        <div className="card__title">ENTREPRENEURS LIKE YOU NEED TO DO IT ALL</div>
                        <div className="card__sub-title">YOU’VE GOT YOUR WORK CUT OUT FOR YOU</div>
                        <div className="card__text">
                            In this business, success doesn’t come easy, and your reputation is on the line. To really win, it takes hard work, the right tools...and a little bit of luck.
                            <br /><br/>
                            It’s natural to feel overwhelmed when you’re responsiblefor finding new leads, making sales, fulfilling orders, and maintaining customer relationships. We think there should be a solution that lets you easily manage the most important parts of your business by providing you with a system for success.
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="build-marketing">
            <div className="section-title gradient-back-left" >CREATE AND SHARE YOUR MESSAGE.<br/> WATCH YOUR BUSINESS GROW.</div>
            <div className="sub-title">There should be a system designed with your do-it-all needs in mind.
                Shuffle helps you elevate your brand, build your network and keep track
                of all your customers. Share your passion and promote your business using real
                data and a done-for-you system that works.
                <br/><br/>
                We’ve helped thousands of business owners like you build something worth sharing
            </div>
            <div className="cards-container">
                <div className="card">
                    <div className="left-fix">
                        <div className="card__content">
                            <div className="card__title">BUILD PROFESSIONAL MARKETING COLLATERAL</div>
                            <div className="card__sub-title">BUILD MARKETING MATERIALS THAT MAKE YOU LOOK LIKE THE PROFESSIONAL WE KNOW YOU ARE</div>
                            <div className="card__text">
                                With Shuffle you won’t just make a great first impression, you’ll always have the right marketing material on hand to share with any prospect.
                            </div>
                            <ul className="card__list">
                                <li className="card__list__item">Build a digital business card</li>
                                <li className="card__list__item">Create mobile landing pages that highlight your brand</li>
                                <li className="card__list__item">Show off your products and services with stunning photos and embedded videos</li>
                                <li className="card__list__item">Share the resources that work with the rest of your team</li>
                            </ul>
                        </div>
                    </div>
                    <img className="card__img" src="/assets/card-1.png" alt="" />
                </div>
                <div className="card">
                    <img className="card__img" src="/assets/card-2.png" alt="" />
                    <div className="right-fix">
                        <div className="card__content">
                            <div className="card__title">GROW YOUR NETWORK AND YOUR SALES</div>
                            <div className="card__sub-title">SHUFFLE MAKES IT EASY TO ADD CONTACTS, UPDATE RECORDS AND SCHEDULE FOLLOW-UPS SO YOUR LEADS AND CONTACTS NEVER FALL THROUGH THE CRACKS</div>
                            <ul className="card__list">
                                <li className="card__list__item">Stay in touch with the people that matter most</li>
                                <li className="card__list__item">Keep notes and set follow-up reminders so you’re always prepared for the next conversation</li>
                                <li className="card__list__item">Touchless and paperless sharing makes face to face encounters easy</li>
                                <li className="card__list__item">Socialize with other entrepreneurs who share resources and best practices</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="left-fix">
                        <div className="card__content">
                            <div className="card__title">TRACK & MEASURE YOUR EFFORTS</div>
                            <div className="card__sub-title">it’s crucial to learn what’s working and what’s not.</div>
                            <div className="card__text">
                            Successful entrepreneurs don’t need “magic” to achieve results. They use data to understand what works, then replicate it over and over again to scale their success. With Shuffle you can do this too.
                            </div>
                            <ul className="card__list">
                                <li className="card__list__item">Track your marketing so you always know how your message is performing</li>
                                <li className="card__list__item">Get real-time notifications when someone views your material</li>
                                <li className="card__list__item">Optimize your results with stats and analytics</li>
                                <li className="card__list__item">Track how much time leads spend on each page and what they’ve clicked on</li>
                            </ul>
                            



                        </div>
                    </div>
                    <img className="card__img" src="/assets/card-3.png" alt="" />
                </div>
            </div>
        </section>
        <section className="get-started">
            <div className="section-title gradient-back-right">GETTING STARTED WITH SHUFFLE IS EASY!</div>
                <section className="icons">
                    <div className="container">
                        <div className="hero__icons">
                            <div className="hero__icon hero__icon--bigger">
                                <img src="./assets/started-1.png" alt="icon" className="hero__icon__image" />
                                <p className="hero__icon__text">Sign up for Shuffle and download the app</p>
                            </div>
                            <div className="hero__icon hero__icon--bigger">
                                <img src="./assets/started-2.png" alt="icon" className="hero__icon__image" />
                                <p className="hero__icon__text">Create your first landing page</p>
                            </div>
                            <div className="hero__icon hero__icon--bigger">
                                <img src="./assets/started-3.png" alt="icon" className="hero__icon__image" />
                                <p className="hero__icon__text">Share it with new contacts and track its success</p>
                            </div>
                        </div>
                        <a className="btn btn--lightblue" href="https://app.elify.com/signup?">LAST CHANCE TO TRY SHUFFLE FOR $1</a>
                        <div className="disclaimer">No obligations, no contracts, cancel at any time.</div>
                    </div>
                </section>
        </section>

        <section className="impressions">
            <div className="impressions__title">WHAT ENTREPRENEURS ARE SAYING ABOUT SHUFFLE</div>
            <div className="container">
                <Swiper
                    navigation
                    loop
                    breakpoints={{
                        1024: {
                            slidesPerView: 3,
                            spaceBetween: 16,
                        },
                        768: {
                            slidesPerView: 2,
                            spaceBetween: 16,
                        },
                        480: {
                            slidesPerView: 1,
                            spaceBetween: 16, 
                        }
                    }}
                    >
                    <SwiperSlide>
                        <>
                            <div className="imressions__text">
                                “SHUFFLE! Shuffle has
                                transformed my life and
                                my time! I can build a
                                card to onboard a new
                                team member (send it out)
                                and keep up with
                                everything my day gives me!
                                It’s a life saver!”
                            </div>
                            <div className="impressions__name">
                                – Jen S.
                            </div>
                        </>
                    </SwiperSlide>
                    <SwiperSlide>
                        <>
                            <div className="imressions__text">
                                “Without Shuffle I’d be lost.
                                Being able to organize my
                                customers into groups
                                allows me to communicate
                                with my customers when
                                it really matters to them.
                                They’re always impressed
                                by how much I remember
                                about them, even if we’ve
                                only interacted once
                                or twice.”
                            </div>
                            <div className="impressions__name">
                                – John B.
                            </div>
                        </>
                    </SwiperSlide>
                    <SwiperSlide>
                        <>
                            <div className="imressions__text">
                                “I love how Shuffle makes
                                it so easy to share my
                                business to potential
                                customers, clients and
                                partners. The ability
                                to set up follow up reminders,
                                take detail notes, run
                                lead capture campaigns
                                and track data allows
                                me to remain in the drivers
                                seat at all times.”
                            </div>
                            <div className="impressions__name">
                                – Bryce C.
                            </div>
                        </>
                    </SwiperSlide>
                </Swiper>
            </div>
        </section>

        <footer className="footer gradient-back-left">
            <div className="container">
                <div className="footer-row">
                    <div className="logo-wrap">
                        <img className="logo" alt="" src="./assets/white-logo.png"/>
                        <div className="elify-text">Smart and fast solutions<br/> to help you succeed.</div>
                    </div>
                    <div className="footer-columns">
                        <div className="footer-column">
                            <div className="column-title">PRODUCTS</div>
                            <div className="column-item"><a href="/">Overview</a></div>
                            <div className="column-item"><a href="/">FAQS</a></div>
                            <div className="column-item"><a href="/">Features</a></div>
                            <div className="column-item"><a href="/">Pricing</a></div>
                        </div>
                        <div className="footer-column">
                            <div className="column-title">COMPANY</div>
                            <div className="column-item"><a href="/">About Us</a></div>
                            <div className="column-item"><a href="/">Contact</a></div>
                        </div>
                        <div className="footer-column">
                            <div className="column-title">SUPPORT</div>
                            <div className="column-item"><a href="/">Insiders</a></div>
                            <div className="column-item"><a href="/">Help Desk</a></div>
                        </div>
                        <div className="footer-column">
                            <div className="column-title">LEGAL</div>
                            <div className="column-item"><a href="/">Terms & <br/>Conditions</a></div>
                            <div className="column-item"><a href="/">Privacy Policy</a></div>
                        </div>
                    </div>
                </div>
                <div className="footer-icons">
                    <a className="footer-icon" href="https://www.instagram.com/elifyshuffle/"><i class="fab fa-instagram"></i></a>
                    <a className="footer-icon" href="https://www.facebook.com/GoElify/"><i class="fab fa-facebook-f"></i></a>
                    <a className="footer-icon" href="https://www.pinterest.com/goelify/"><i class="fab fa-pinterest"></i></a>
                    <a className="footer-icon" href="https://twitter.com/goelify/"><i class="fab fa-twitter"></i></a>
                    <a className="footer-icon" href="https://www.youtube.com/elify/"><i class="fab fa-youtube"></i></a>
                    <a className="footer-icon" href="https://www.linkedin.com/company/elify/"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <div className="reserved">
                    ©2020 SHUFFLE By Elify, All rights reserved    
                </div>
            </div>
        </footer>
    </div>

    )
}

export default Home